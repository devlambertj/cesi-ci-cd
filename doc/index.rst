.. CESI CI CD documentation master file, created by
   sphinx-quickstart on Thu May  4 11:15:47 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to CESI CI CD's documentation!
======================================

.. image:: dog.png

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   test-1

Nous allons parler de :term:`CI/CD` et de :term:`Devops`


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

